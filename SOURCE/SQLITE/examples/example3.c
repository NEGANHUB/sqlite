#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h" 

     
int main(void) {
    sqlite3 *db;
    sqlite3_stmt *res;
    int rc = 0;
    int rec_count = 0;
    const char *errMSG;
    const char *tail;
	

    rc = sqlite3_open("ex3.db", &db);
    if (rc) {
            puts("Can not open database");
            exit(0);
    }

    // Create a new table called people in database     
    rc = sqlite3_exec(db,"create table people (id integer, firstname varchar(20), \
		     lastname varchar(20), phonenumber char(10))", 0, 0, 0);

    //rc = sqlite3_exec(db,"delete from people", 0, 0, 0); //erase previous inserts

    rc = sqlite3_exec(db,"insert into people (id, firstname, lastname, phonenumber) \
		     values (1, 'Fred', 'Flintstone', '5055551234');", NULL , NULL , NULL);

    rc = sqlite3_exec(db,"insert into people (id, firstname, lastname, phonenumber) \
		     values (2, 'Wilma', 'Flintstone', '5055551234');" , NULL , NULL , NULL);

    rc = sqlite3_exec(db,"insert into people (id, firstname, lastname, phonenumber) \
		     values (3, 'Barny', 'Rubble', '5055554321');" , NULL , NULL , NULL);

    rc = sqlite3_exec(db,"update people set phonenumber=\'5055559999\' where id=3", 0, 0, 0);
    
    rc = sqlite3_prepare_v2(db,"select lastname,firstname,phonenumber,id from people order by id", 
                      1000, &res, &tail);
    
    if (rc != SQLITE_OK) {
                      puts("We did not get any data!");
                      exit(0);
    }
    puts("==========================");
    
    while (sqlite3_step(res) == SQLITE_ROW) {
                      printf("%s|", sqlite3_column_text(res, 0));
                      printf("%s|", sqlite3_column_text(res, 1));
                      printf("%s|", sqlite3_column_text(res, 2));
                      printf("%u\n", sqlite3_column_int(res, 3));
    
                      rec_count++;
    }
    
    puts("==========================");
    printf("We received %d records.\n", rec_count);
    
    sqlite3_finalize(res);
    
    sqlite3_close(db);
    
    return 0;
}
