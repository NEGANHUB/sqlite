#include <iostream>
#include <stdlib.h>
#include "sqlite3.h"
 
sqlite3 *database;       
 
using namespace std;
 
int Callback( void *Pointer, int argc, char **argv, char **columnNames)
// (pointer to database,number of columns retrieved,
// array of pointers to the strings in the fields,column name)
{
  cout<< *argv <<endl;       // prints all entries in column "entry" 
  return 0;
}
 
 
int main(int argc, char *argv[])
{
  // open database
  int resultcode = sqlite3_open( "mydatabase" , &database);

  //create table and insert entries
  sqlite3_exec( database , "create table test(number integer , entry varchar(50) , primary key(number));" , NULL , NULL , NULL);
  sqlite3_exec( database , "insert into test (number , entry) values (1 , 'Testentry 0');" , NULL , NULL , NULL);
  sqlite3_exec( database , "insert into test (number , entry) values (2 , 'Testentry 1');" , NULL , NULL , NULL);
  sqlite3_exec( database , "insert into test (number , entry) values (3 , 'Testentry 2');" , NULL , NULL , NULL);
 
  // query database: retrieve all fields in column "entry":
  sqlite3_exec( database , "select entry from test;" , Callback , NULL , NULL);
 
  // close database again
  sqlite3_close(database);
 
  return 0;
}
